Weather for STRV
================

This is a test project for STRV written in Swift 3 using CocoaPods. App supports iOS 9.0+ and is designed for iPhone. It uses Open Weather Map API (http://openweathermap.org/api).

Note:
-----

According to STRV guidelines it is mandatory to use SSL connection to backend in release configuration. Open Wather API does NOT supoort SSL in a free plan. This demo app uses unsecure connection to thi API.