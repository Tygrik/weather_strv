//
//  WeatherFetcher.swift
//  Weather
//
//  Created by Petr Holub on 2017-02-19.
//  Copyright © 2017 holub.petr. All rights reserved.
//

import Foundation
import SwiftyJSON

class WeatherFetcher {
    let networking: Networking
    let units = "metric"
    
    init(networking: Networking) {
        self.networking = networking
    }
    
    func fetchTodayForLocation(_ location: (latitude: Double, longitude: Double), response: @escaping (WeatherToday?, Error?) -> ()) {
        let type = "weather"
        
        networking.request(path: type, parameters: paramsFromLocation(location)) { (data, error) in
            if let data = data {
                let jsonData = JSON(data)
                response(WeatherToday(jsonData), nil)
            } else {
                response(nil, error)
            }
        }
    }
    
    func fetchForecastForLocation(_ location: (latitude: Double, longitude: Double), response: @escaping ([WeatherForecast]?, Error?) ->()) {
        let type = "forecast/daily"
        
        networking.request(path: type, parameters: paramsFromLocation(location)) { (data, error) in
             if let data = data {
                let jsonData = JSON(data)
                var forecast: [WeatherForecast] = []
                for day in jsonData["list"] {
                    forecast.append(WeatherForecast(day.1))
                }
                response(forecast, nil)
             } else {
                response(nil, error)
             }
        }
    }
    
    func paramsFromLocation(_ location: (latitude: Double, longitude: Double)) -> Dictionary<String, Any> {
        var parameters: Dictionary<String, Any> = [:]
        parameters["lat"] = String(format:"%.3f", location.latitude)
        parameters["lon"] = String(format:"%.3f", location.longitude)
        parameters["units"] = units
        
        return parameters
    }
}

