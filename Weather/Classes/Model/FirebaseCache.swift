//
//  FirebaseCache.swift
//  Weather
//
//  Created by Petr Holub on 2017-02-19.
//  Copyright © 2017 holub.petr. All rights reserved.
//

import UIKit
import Firebase

protocol FirebaseCacheDelegate: class {
    func locationChanged(_ location: (latitude: Double, longitude: Double))
}

class FirebaseCache: DataStoring {
    
    weak var delegate:FirebaseCacheDelegate?
    var database: FIRDatabase?
    var vendorUID: String? {
        return UIDevice.current.identifierForVendor?.uuidString
    }
    
    // MARK: - Start of lifecycle
    
    init(withDatabase database: FIRDatabase) {
        self.database = database
        observeLocationChange()
    }
    
    // MARK: - Location persistance
    
    func saveUserLocation(_ location: (latitude: Double, longitude: Double)) {
        guard let userRef = userRef() else {
            return
        }
        
        userRef.child("location").setValue(["latitude": location.latitude, "longitude": location.longitude])
    }
    
    func saveTemperatureAtUserPlace(_ temperature: Double) {
        guard let userRef = userRef() else {
            return
        }
        
        userRef.child("temperature").setValue(temperature)
    }
    
    func observeLocationChange() {
        if let userRef = userRef() {
            userRef.child("location").observe(.value, with: { snapshot in
                let latitudeDouble = snapshot.childSnapshot(forPath: "latitude").value as? Double
                let longitudeDouble = snapshot.childSnapshot(forPath: "longitude").value as? Double
                
                if let latitude = latitudeDouble, let longitude = longitudeDouble {
                    self.delegate?.locationChanged((latitude, longitude))
                }
            })
        }
    }
    
    func userRef() -> FIRDatabaseReference? {
        guard let vendorDeviceId = self.vendorUID, let database = self.database else {
            return nil
        }
        
        return database.reference().child(vendorDeviceId)
    }
}
