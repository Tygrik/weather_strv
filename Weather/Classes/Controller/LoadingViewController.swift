//
//  LoadingViewController.swift
//  Weather
//
//  Created by Petr Holub on 2017-02-23.
//  Copyright © 2017 holub.petr. All rights reserved.
//

import UIKit

class LoadingViewController: UIViewController {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var statusTextLabel: UILabel!
    
    func displayError(_ error: String) {
        self.view.isHidden = false
        statusTextLabel.text = error
        activityIndicator.isHidden = true
    }
}
