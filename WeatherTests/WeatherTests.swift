//
//  WeatherTests.swift
//  WeatherTests
//
//  Created by Petr Holub on 2017-02-18.
//  Copyright © 2017 holub.petr. All rights reserved.
//

import XCTest
@testable import Weather

class WeatherTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testNetworking() {
        let network = Network()
        var parameters:Dictionary<String, Any> = [:]
        parameters["q"] = "London,uk"
        
        let completitionExpectation = self.expectation(description: "API should return data and no errors")
        
        network.request(path: "weather", parameters: parameters) { (data, error) in
            if data != nil { completitionExpectation.fulfill() }
            XCTAssertNotNil(data, "Data are empty")
            XCTAssertNil(error, "Error received")
        }
        
        self.waitForExpectations(timeout: 5.0, handler: nil)
    }
    
}
