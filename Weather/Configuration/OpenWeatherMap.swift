//
//  Configuration.swift
//  Weather
//
//  Created by Petr Holub on 2017-02-18.
//  Copyright © 2017 holub.petr. All rights reserved.
//

import Foundation

struct OpenWeatherMap {
    static let appIdKey = "appid"
    static let appIdValue = ""
    static let baseUrl = "http://api.openweathermap.org/data/2.5/"
}
