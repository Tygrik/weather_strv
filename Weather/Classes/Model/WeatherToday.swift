//
//  WeatherForecastDay.swift
//  Weather
//
//  Created by Petr Holub on 2017-02-19.
//  Copyright © 2017 holub.petr. All rights reserved.
//

import Foundation
import SwiftyJSON

struct WeatherToday {
    let description: String
    let icon: String
    let place: String
    let humidity: Double
    let preasure: Double
    let temperature: Double
    let windSpeed: Double
    let windDirection: Double
    
    init(_ data: JSON) {
        description = data["weather"][0]["description"].stringValue
        icon = data["weather"][0]["icon"].stringValue
        place = data["name"].stringValue
        humidity = data["main"]["humidity"].doubleValue
        preasure = data["main"]["pressure"].doubleValue
        windSpeed = data["wind"]["speed"].doubleValue
        windDirection = data["wind"]["deg"].doubleValue
        temperature = data["main"]["temp"].doubleValue
    }
}

extension Double {
    func returningCentigrades() -> String {
        return String(format:"%.0f°C", self)
    }
    
    func returningHectopascals() -> String {
        return String(format:"%.0f hPa", self)
    }
    
    func returningPercents() -> String {
        return String(format:"%.0f%%", self)
    }
    
    func returningKilometersPerHour() -> String {
        return String(format:"%.0f km/h", self * 3.6)
    }
    
    func returningWindDirection() -> String {
        if (self >= 11.25 && self < 33.75) { return "NNE" }
        if (self >= 33.75 && self < 56.25) { return "NE" }
        if (self >= 56.25 && self < 78.75) { return "ENE" }
        if (self >= 78.75 && self < 101.25) { return "E" }
        if (self >= 101.25 && self < 123.75) { return "NSE" }
        if (self >= 123.75 && self < 146.25) { return "SE" }
        if (self >= 146.25 && self < 168.75) { return "SSE" }
        if (self >= 168.75 && self < 191.25) { return "S" }
        if (self >= 191.25 && self < 213.75) { return "SSW" }
        if (self >= 213.75 && self < 236.25) { return "SW" }
        if (self >= 236.25 && self < 258.75) { return "WSW" }
        if (self >= 258.75 && self < 281.25) { return "W" }
        if (self >= 281.25 && self < 303.75) { return "WNW" }
        if (self >= 303.75 && self < 326.25) { return "NW" }
        if (self >= 326.25 && self < 348.75) { return "NNW" }
        return "N"
    }
}
