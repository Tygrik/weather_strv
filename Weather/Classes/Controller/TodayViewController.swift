//
//  TodayViewController.swift
//  Weather
//
//  Created by Petr Holub on 2017-02-22.
//  Copyright © 2017 holub.petr. All rights reserved.
//

import UIKit

class TodayViewController: UIViewController {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var conditionsLabel: UILabel!
    @IBOutlet weak var rainProbabilityLabel: UILabel!
    @IBOutlet weak var rainAmountLabel: UILabel!
    @IBOutlet weak var presureLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var windDirectionLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    var loaded = false
    var today: WeatherToday? {
        didSet {
            if loaded {
                displayData()
            }
        }
    }
    
    var error: String? {
        didSet {
            if loaded {
                loadingViewController()?.displayError(error!)
            }
        }
    }
    
    override func viewDidLoad() {
        displayData()
        displayError()
        loaded = true;
    }
    
    func displayData() {
        if let today = today {
            locationLabel.text = today.place
            presureLabel.text = today.preasure.returningHectopascals()
            rainAmountLabel.text = "N/A"
            rainProbabilityLabel.text = "N/A"
            windSpeedLabel.text = today.windSpeed.returningKilometersPerHour()
            windDirectionLabel.text = today.windDirection.returningWindDirection()
            if let iconName = today.icon.returningIconName() {
                iconImageView.image = UIImage.init(named: iconName + "_Big")
            }
            conditionsLabel.text = String(format:"%.0f°C | %@", today.temperature, today.description.capitalizingFirstLetter())
            containerView.isHidden = true;
        }
    }
    
    func displayError() {
        if let error = error {
            loadingViewController()?.displayError(error)
        }
    }
    
    @IBAction func shareTapped(_ sender: Any) {
        if let today = today {
            shareText("Oh wow! it is \(today.temperature.returningCentigrades()) at my place!")
        }
    }
    
    func shareText(_ text: String) {
        let vc = UIActivityViewController(activityItems: [text], applicationActivities: [])
        present(vc, animated: true, completion: nil)
    }
    
}

extension TodayViewController {
    func loadingViewController() -> LoadingViewController? {
        for vc in self.childViewControllers {
            if vc is LoadingViewController {
                return vc as? LoadingViewController
            }
        }
        
        return nil;
    }
}
