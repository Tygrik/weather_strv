//
//  WeatherForecastDay.swift
//  Weather
//
//  Created by Petr Holub on 2017-02-19.
//  Copyright © 2017 holub.petr. All rights reserved.
//

import Foundation
import SwiftyJSON

struct WeatherForecast {
    let description: String
    let icon: String
    let temperature: Double
    let date: Date
    
    init(_ data: JSON) {
        description = data["weather"][0]["description"].stringValue
        icon = data["weather"][0]["icon"].stringValue
        temperature = data["temp"]["day"].doubleValue
        date = Date(timeIntervalSince1970: data["dt"].doubleValue)
    }
}

extension String {
    func capitalizingFirstLetter() -> String {
        let first = String(characters.prefix(1)).capitalized
        let other = String(characters.dropFirst())
        return first + other
    }
    
    func returningIconName() -> String? {
        var iconName:String?
        switch self {
        case "50d", "50n", "09d", "09n", "04d", "04n", "03d", "03n":
            iconName = "Cloudy"
        case "01d", "01n":
            iconName = "Sun"
        case "13d", "13n", "11d", "11n", "10d", "10n":
            iconName = "Lightning"
        default:
            iconName = nil
        }
        
        return iconName
    }
}

extension Date {
    func dayOfWeek() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self)
    }
}
