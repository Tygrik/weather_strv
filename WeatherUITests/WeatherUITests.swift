//
//  WeatherUITests.swift
//  WeatherUITests
//
//  Created by Petr Holub on 2017-02-18.
//  Copyright © 2017 holub.petr. All rights reserved.
//

import XCTest

class WeatherUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        XCUIApplication().launch()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testTabSwitching() {
        XCUIDevice.shared().orientation = .portrait
        
        let tabBarsQuery = XCUIApplication().tabBars
        tabBarsQuery.buttons["Forecast"].tap()
        tabBarsQuery.buttons["Today"].tap()
        
    }
    
    func testSharing() {
        XCUIDevice.shared().orientation = .portrait
        
        let app = XCUIApplication()
        let shareButton = app.buttons["Share"]
        shareButton.tap()
        
        let collectionViewsQuery = app.collectionViews.collectionViews
        collectionViewsQuery.buttons["Message"].tap()
        
        let cancelButton = app.navigationBars["New Message"].buttons["Cancel"]
        cancelButton.tap()
        shareButton.tap()
        collectionViewsQuery.buttons["Mail"].tap()
        cancelButton.tap()
        app.sheets.buttons["Delete Draft"].tap()
        
    }
    
}
