//
//  TabbarController.swift
//  Weather
//
//  Created by Petr Holub on 2017-02-19.
//  Copyright © 2017 holub.petr. All rights reserved.
//

import UIKit
import CoreLocation

class TabbarController: UITabBarController {
    
    var todayViewController: TodayViewController?
    var forecastViewController: ForecastViewController?
    
    private lazy var locationManager: CLLocationManager = {
        let locationManager = CLLocationManager()
        locationManager.desiredAccuracy = 1000.0
        locationManager.distanceFilter = 1000.0
        
        return locationManager
    }()
    
    var loaded = false
    var locationAvailable = false
    
    var weatherFetcher: WeatherFetcher?
    var cache: DataStoring? {
        didSet {
            (cache as! FirebaseCache).delegate = self
        }
    }
    
    // MARK: Start of lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        todayViewController = self.viewControllers![0] as? TodayViewController
        forecastViewController = self.viewControllers![1] as? ForecastViewController
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getUserLocation()
    }
    
    
    // MARK: - User location handling
    
    public func getUserLocation() {
        locationManager.delegate = self
        
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
        else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    // MARK: - Weather loading
    
    fileprivate func loadWeatherForLocation(_ location: (latitude: Double, longitude: Double)) {
        weatherFetcher?.fetchTodayForLocation(location, response: { (data, error) in
            if let data = data {
                if let cache = self.cache {
                    cache.saveTemperatureAtUserPlace(data.temperature)
                }
                if let todayViewController = self.todayViewController {
                    todayViewController.today = data
                }
            } else {
                if let todayViewController = self.todayViewController {
                    todayViewController.error = "Unable to load data"
                }
            }
        })
        weatherFetcher?.fetchForecastForLocation(location, response: { (data, error) in
            if let data = data {
                if let forecastViewController = self.forecastViewController {
                    forecastViewController.forecastDays = data
                }
            } else {
                if let forecastViewController = self.forecastViewController {
                    forecastViewController.error = "Unable to load data"
                }
            }
        })
    }
    
}

// MARK: - Location manager delegate

extension TabbarController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            manager.requestLocation()
        }
        else {
            handleLocationError(authorized: false)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            guard let cache = self.cache else {
                return
            }
            cache.saveUserLocation((latitude: location.coordinate.latitude, longitude: location.coordinate.longitude))
            manager.delegate = nil
            manager.stopUpdatingLocation()
        }
        else {
            handleLocationError(authorized: true)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        handleLocationError(authorized: true)
    }
    
    func handleLocationError(authorized: Bool) {
        if authorized {
            if !locationAvailable {
                displayLocationErrors()
            }
        }
        else {
            displayLocationErrors()
        }
    }
    
    func displayLocationErrors() {
        if let forecastViewController = self.forecastViewController {
            forecastViewController.error = "Unable to get location"
        }
        if let todayViewController = self.todayViewController {
            todayViewController.error = "Unable to get location"
        }
    }
}

// MARK: - Firebase cache delegate

extension TabbarController: FirebaseCacheDelegate {
    func locationChanged(_ location: (latitude: Double, longitude: Double)) {
        loadWeatherForLocation(location);
        locationAvailable = true
    }
}
