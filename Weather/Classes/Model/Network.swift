//
//  Network.swift
//  Weather
//
//  Created by Petr Holub on 2017-02-19.
//  Copyright © 2017 holub.petr. All rights reserved.
//

import Foundation
import Alamofire

class Network : Networking {
    let manager: SessionManager
    
    init() {
        let config = URLSessionConfiguration.default
        config.requestCachePolicy = .returnCacheDataElseLoad
        config.urlCache = URLCache.shared
        manager = Alamofire.SessionManager(configuration: config)
    }
    
    func request(path: String, parameters: Dictionary<String, Any>, responseClojure: @escaping (Any?, Error?) -> ()) {
        var parameters = parameters
        parameters[OpenWeatherMap.appIdKey] = OpenWeatherMap.appIdValue
        
        manager.request(OpenWeatherMap.baseUrl + path, parameters: parameters)
            .validate()
            .responseJSON { (response) -> Void in
                print(response)
                
                switch response.result {
                case .success(let value):
                    responseClojure(value, nil)
                case .failure(let error):
                    responseClojure(nil, error)
                }
        }
    }
}
