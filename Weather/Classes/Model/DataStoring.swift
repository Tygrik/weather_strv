//
//  DataStoring.swift
//  Weather
//
//  Created by Petr Holub on 2017-02-19.
//  Copyright © 2017 holub.petr. All rights reserved.
//

import Foundation

protocol DataStoring {
    func saveUserLocation(_ location: (latitude: Double, longitude: Double))
    func saveTemperatureAtUserPlace(_ temperature: Double)
}
