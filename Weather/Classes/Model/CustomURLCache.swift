//
//  CustomURLCache.swift
//  Weather
//
//  Created by Petr Holub on 2017-02-20.
//  Copyright © 2017 holub.petr. All rights reserved.
//

import Foundation

class CustomURLCache: URLCache {

    let customURLCacheExpirationKey = "CustomMeteoriteURLCacheExpiration"
    let expirationInterval: TimeInterval = 600
        
    override func cachedResponse(for request: URLRequest) -> CachedURLResponse? {
        if let cachedResponse = super.cachedResponse(for: request) {
            if let userInfo = cachedResponse.userInfo {
                let cacheDate = userInfo[customURLCacheExpirationKey] as! Date
                let cacheExpirationDate = cacheDate.addingTimeInterval(expirationInterval)
                if cacheExpirationDate.compare(Date()) == .orderedAscending {
                    removeCachedResponse(for: request)
                    return nil
                }
            }
            return cachedResponse
        }
        return nil
    }
    
    override func storeCachedResponse(_ cachedResponse: CachedURLResponse, for request: URLRequest) {
        var modifiedUserInfo: [AnyHashable:Any] = [:]
        
        if let userInfo = cachedResponse.userInfo {
            modifiedUserInfo = userInfo
        }
        
        modifiedUserInfo[customURLCacheExpirationKey] = Date()
        let modifiedCachedResponse = CachedURLResponse.init(response: cachedResponse.response, data: cachedResponse.data, userInfo: modifiedUserInfo, storagePolicy: cachedResponse.storagePolicy)
        super.storeCachedResponse(modifiedCachedResponse, for: request)
        
    }
}
