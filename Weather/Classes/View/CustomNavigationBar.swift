//
//  CustomNavigationBar.swift
//  Weather
//
//  Created by Petr Holub on 2017-02-21.
//  Copyright © 2017 holub.petr. All rights reserved.
//

import UIKit

class CustomNavigationBar: UINavigationBar {
    let lineView: UIImageView
    
    required init?(coder aDecoder: NSCoder) {
        lineView = UIImageView.init(image: UIImage.init(named: "Line"))
        super.init(coder: aDecoder)
        
        self.setBackgroundImage(UIImage(), for: .default)
        self.shadowImage = UIImage()
        self.addSubview(lineView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        guard let line = lineView.image else {
            return
        }
        
        let multiplier = self.frame.size.width / line.size.width
        let finalHeight = line.size.height * multiplier
        let finalWidth = line.size.width * multiplier
        
        lineView.frame = CGRect.init(x: 0, y: self.frame.height - finalHeight, width: finalWidth, height: finalHeight)
    }
}
