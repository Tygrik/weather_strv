//
//  ForecastCell.swift
//  Weather
//
//  Created by Petr Holub on 2017-02-22.
//  Copyright © 2017 holub.petr. All rights reserved.
//

import UIKit

class ForecastCell: UITableViewCell {
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var conditionLabel: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.separatorInset = UIEdgeInsets.init(top: 0, left: 85, bottom: 0, right: 0)
    }
    
}
