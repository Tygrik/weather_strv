//
//  ForecastViewController.swift
//  Weather
//
//  Created by Petr Holub on 2017-02-22.
//  Copyright © 2017 holub.petr. All rights reserved.
//

import UIKit

class ForecastViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var containerView: UIView!
    var loaded = false
    
    var forecastDays: Array<WeatherForecast>? {
        didSet {
            if loaded {
                tableView.reloadData()
                containerView.isHidden = true;
            }
        }
    }
    
    var error: String? {
        didSet {
            if loaded {
                loadingViewController()?.displayError(error!)
            }
        }
    }
    
    override func viewDidLoad() {
        setupTableView()
        displayError()
    }
    
    func setupTableView() {
        tableView.allowsSelection = false;
        tableView.dataSource = self
        tableView.reloadData()
        loaded = true;
        
        if (forecastDays != nil) {
            containerView.isHidden = true;
        }
    }
    
    func displayError() {
        if let error = error {
            loadingViewController()?.displayError(error)
        }
    }
    
}


// MARK: - TableView Data Source

extension ForecastViewController: UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let forecastDays = forecastDays {
            return forecastDays.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ForecastCell", for: indexPath) as! ForecastCell
        
        cell.conditionLabel.text = forecastDays![indexPath.row].description.capitalizingFirstLetter()
        cell.dayLabel.text = forecastDays![indexPath.row].date.dayOfWeek()?.capitalizingFirstLetter()
        cell.temperatureLabel.text = String(format:"%.0f°", forecastDays![indexPath.row].temperature)
        if let icon = forecastDays![indexPath.row].icon.returningIconName() {
            cell.iconImageView.image = UIImage.init(named: icon)
        }
        
        
        return cell
    }
    
}

extension ForecastViewController {
    func loadingViewController() -> LoadingViewController? {
        for vc in self.childViewControllers {
            if vc is LoadingViewController {
                return vc as? LoadingViewController
            }
        }
        
        return nil;
    }
}
