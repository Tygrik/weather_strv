//
//  AppDelegate.swift
//  Weather
//
//  Created by Petr Holub on 2017-02-18.
//  Copyright © 2017 holub.petr. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        setupCache()
        let controller = mainTabbarController();
        injectDependenciesIntoController(controller)
        
        return true
    }
    
    func mainTabbarController() -> TabbarController {
        let tabbarController = UIStoryboard.init(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "TabbarController") as! TabbarController
        window = UIWindow.init(frame: UIScreen.main.bounds)
        window?.rootViewController = tabbarController
        window?.makeKeyAndVisible()
        
        return tabbarController
    }
    
    func setupFireBase() -> FIRDatabase {
        let filePath = Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist")!
        let options = FIROptions(contentsOfFile: filePath)!
        FIRApp.configure(with: options)
        FIRDatabase.database().persistenceEnabled = true
        
        return FIRDatabase.database()
    }
    
    func injectDependenciesIntoController(_ controller: TabbarController) {
        let firebase = setupFireBase()
        let cache = FirebaseCache(withDatabase: firebase)
        let networking = Network()
        let weatherFetcher = WeatherFetcher(networking: networking)
        
        controller.weatherFetcher = weatherFetcher
        controller.cache = cache
    }
    
    func setupCache() {
        let memoryCapacity = 50 * 1024 * 1024
        let diskCapacity = 50 * 1024 * 1024
        let cache = CustomURLCache(memoryCapacity: memoryCapacity, diskCapacity: diskCapacity, diskPath: "myDataPath")
        URLCache.shared = cache
    }
}

