//
//  Networking.swift
//  Weather
//
//  Created by Petr Holub on 2017-02-19.
//  Copyright © 2017 holub.petr. All rights reserved.
//

import Foundation

protocol Networking {
    func request(path: String, parameters: Dictionary<String, Any>, responseClojure: @escaping (Any?, Error?) -> ())
}
